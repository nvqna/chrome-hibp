// content.js

// find out the current tab id
var tabId;
chrome.extension.sendMessage({ type: 'getTabId' }, function(res) {
    tabId = res.tabId;
	console.log('tabId ' + tabId);
});

url = window.location.toString();

// get all email addresses on the page
// shamelessly stolen from https://stackoverflow.com/questions/22997516/to-find-all-emails-on-a-page
var search_in = document.body.innerHTML;
string_context = search_in.toString();
// match returns all results including all duplicates, so cast to a set and then back to a list.
array_mails = [...new Set(string_context.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi))];

// query hibp api
// docs here: https://haveibeenpwned.com/API/v3
// GET https://haveibeenpwned.com/api/v3/breachedaccount/{account}
var baseURL = "https://haveibeenpwned.com/api/v3/breachedaccount/";
//var baseURL = "http://127.0.0.1:6789/api/v2/breachedaccount/" 
var truncate = "?truncateResponse=true";
var data={};
// put this info in a dict with the tabID as key
// popup.js can use the tabID to only show data
// for the current tab.
// localstorage is key:value.
// key is tabID, value is tabData dict
var tabData={};
var tab = {}
var url = window.location.toString();


/*
object with emails as keys, breachlist as value (list)
[email1:[breachlist], email2:[breachlist], email3:[breachlist],...]
*/
var urlstring;
if (array_mails != null) {

	// Limit the number of emails we check. This value is set on the options
	// page. If it's not set, default to 20.
	chrome.storage.local.get(null, function(items) {
		var maxEmails=20;
		if (typeof items.maxEmails!= 'undefined') {
			maxEmails = items.maxEmails;
		}
		array_mails.splice(maxEmails);
		//console.log('checking for a maximum of ' + array_mails.length + 'emails');
	});


    chrome.runtime.sendMessage(
        {contentScriptQuery: "emailList", emails: array_mails,});
}


// helper function
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

// sets the icon on the extension badge.
function setBadge(emailsBreached) {
	chrome.runtime.sendMessage({numEmails:emailsBreached}, function (response) {
		console.log('sent ' + emailsBreached + ' to background script'); 
		console.log('bg: numEmails: %o' , message);   
	});
};
