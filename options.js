//options.js

// Saves options to chrome.storage.local
function save_options() {
  var maxEmails = document.getElementById('maxEmails').value;
  chrome.storage.local.set({maxEmails: maxEmails}, function() {
    console.log('set maxEmails to be ' + maxEmails);
    // Update status to let user know options were saved.
    chrome.runtime.sendMessage({greeting:"fetchmodel"}) ;
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 500);
  });
}


function restore_options() {
  chrome.storage.local.get({
    maxEmails: 'max emails',
  }, function(items) {
    document.getElementById('maxEmails').value = items.maxEmails;
    console.log(items.maxEmails);
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);