// background.js

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {
        if ( message.type == 'getTabId' ){
            sendResponse({ tabId: sender.tab.id });
        }
    }
);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.contentScriptQuery == "emailList") {
        tabid = sender.tab.id;            
        tabURL = sender.tab.url; 
        for (let i=0; i<request.emails.length; i++){
            checkEmail(request.emails[i]);
        }
    }
});



function checkEmail(email){
    console.log('SENDER: ' + tabid);            
    console.log('SENDER: ' + tabURL); 
    console.log('checking ' + email);

    var myHeaders = new Headers({
        'hibp-api-key': '79b51cec465341f0a661e49263f778ed',
        'User-Agent': 'HaveTheyBeenPwned v0.8'
    });

    var urlstring = "https://haveibeenpwned.com/api/v3/breachedaccount/" + email + "?truncateResponse=true";

var data={};
// put this info in a dict with the tabID as key
// popup.js can use the tabID to only show data
// for the current tab.
// localstorage is key:value.
// key is tabID, value is tabData dict
var tabData={};
var tab = {}  

    fetch(urlstring, {headers: myHeaders})
        .then(
            function(response) {
                if (response.status == 404) {
                    //console.log('Email not found ' + response.status);
                return;
                }
                response.json().then(function(result) {
                    //console.log(result);
                var breachlist = []
                for (var j in result){
                    breachlist.push(result[j].Name);
                    //console.log(email + ':' + result[j].Name + ' added to array');
                }
                data[email] = breachlist;


                tab[tabURL] = data;
                //console.log('checkEmail set data: ' + data[email]);

                chrome.storage.local.set(tab);
                console.log('set ' + JSON.stringify(tab))
                })
            })
}

     
//chrome.storage.local.get(function(result){console.log(result)})

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {
        chrome.storage.local.get(null, function(items) {
            chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
                var url = tabs[0].url;
                //console.log(url);
                if (typeof items[url] != 'undefined'){
                    chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
                    chrome.browserAction.setBadgeText({tabId:sender.tab.id, text: Object.keys(items[url]).length.toString()});
                    console.log("setting badge to " + Object.keys(items[url]).length.toString())
                } else {
                    //console.log('Setting badge to zero');
                    chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
                    chrome.browserAction.setBadgeText({text: '0'});
                }

            });
        });
});
